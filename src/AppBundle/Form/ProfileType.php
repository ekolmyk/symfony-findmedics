<?php

namespace AppBundle\Form;

use AppBundle\Entity\Profile;
use AppBundle\Entity\Specialty;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\EasyAdminAutocompleteType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('login')
                ->add('email', EmailType::class )
                ->add('type',  ChoiceType::class, array(
                    'choices'  => array(
                        'Clinic' => 'clinic',
                        'Patient' => 'patient',
                    ))
                )
                ->add('phone')
                ->add('postcode')
                ->add('imageFile', VichImageType::class, array(
                    'required' => false,
                ) )
                ->add('overview', CKEditorType::class, array(
                    'required' => false
                ))
//                ->add('specialties', Select2EntityType::class, array(
//                    'multiple' => true,
//                    'remote_route' => 'tetranz_test_default_countryquery',
//                    'class' => 'AppBundle\Entity\Specialty',
//                    'primary_key' => 'id',
//                    'text_property' => 'name',
//                    'minimum_input_length' => 2,
//                    'page_limit' => 10,
//                    'allow_clear' => true,
//                    'delay' => 250,
//                    'cache' => true,
//                    'cache_timeout' => 60000, // if 'cache' is true
//                    'language' => 'en',
//                    'placeholder' => 'Select a Specialty',
//                ))
                ->add('password', PasswordType::class,
                    array(
                        'always_empty' => false,
                        'required' => false
                    )
                )
                ->add('available24Hours', CheckboxType::class, array(
                    'label' => 'Available for 24 hours',
                    'required' => false
                ))
                ->add('onlineConsultation', CheckboxType::class, array(
                    'required' => false
                ))
                ->add('doesNotRequireGPreferral', CheckboxType::class, array(
                    'label' => 'Doesn\'t require GP referral',
                    'required' => false
                ))
                ->add('status', ChoiceType::class, array(
                    'choices'  => array(
                        'Active' => 1,
                        'Inactive' => 0,
                    ))
                );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profile';
    }


}
