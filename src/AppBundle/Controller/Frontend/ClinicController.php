<?php
namespace AppBundle\Controller\Frontend;

use AppBundle\AppBundle;
use AppBundle\Entity\Clinic;
use AppBundle\Entity\Doctor;
use AppBundle\Form\ProfileType;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Twig\Profiler\Profile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Form;
use EasyCorp\Bundle\EasyAdminBundle\Form\Util\LegacyFormHelper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class ClinicController
 *
 * @Route("/clinic")
 * @package AppBundle\Controller\Frontend
 */
class ClinicController extends Controller
{
    /**
     * @Route("/", name="clinic_index")
     */
    public function indexAction()
    {
        $clinicObj = $this->getDoctrine()->getRepository(Clinic::class)->findAllClinic();

        $encoders = array(new XmlEncoder(), new JsonEncoder());

        $objectNormalizer = new ObjectNormalizer();

        $normalizers = array($objectNormalizer);
        $serializer = new Serializer($normalizers, $encoders);

        $jsonContent = $serializer->serialize($clinicObj[0], 'json');

        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }
}