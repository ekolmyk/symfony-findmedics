<?php

namespace AppBundle\Controller\Frontend\Api\v1;

use AppBundle\Entity\Clinic;
use AppBundle\Entity\Profile;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/api/v1/search")
 */
class SearchController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * Needed for client-side navigation after initial page load.
     */
    public function searchAction(Request $request)
    {
        $optionsResolver = new OptionsResolver();
        $optionsResolver->setDefaults(array(
            'postcode' => '',
            'onlineConsultation' => '',
            'doesNotRequireGPreferral' => '',
            'available24Hours' => '',
            'profileType' => ''
        ));
        $filter = $optionsResolver->resolve($request->query->all());

        $repository = $this->getDoctrine()->getRepository(Profile::class);

        /** @var Profile[] $profiles */
        $profiles = $repository->findFilteredProfiles($filter);

        return new JsonResponse($this->normalizeObjectsJson($profiles));
    }
}