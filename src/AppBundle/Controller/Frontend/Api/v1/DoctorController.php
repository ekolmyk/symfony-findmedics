<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 18.12.17
 * Time: 16:21
 */

namespace AppBundle\Controller\Frontend\Api\v1;


use AppBundle\Entity\Doctor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api/v1/doctor")
 */
class DoctorController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * Needed for client-side navigation after initial page load
     */
    public function doctors(Request $request)
    {
        $doctors = $this->getDoctrine()->getRepository(Doctor::class)->findAllDoctors();

        return new JsonResponse($this->normalizeObjectsJson($doctors));
    }

    /**
     * @Route("/{id}")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * Needed for client-side navigation after initial page load
     */
    public function doctor($id, Request $request)
    {
        $doctors = $this->getDoctrine()->getRepository(Doctor::class)->find($id);

        return new JsonResponse($this->normalizeObjectsJson($doctors));
    }
}