<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 18.12.17
 * Time: 16:21
 */

namespace AppBundle\Controller\Frontend\Api\v1;


use AppBundle\Entity\Clinic;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/api/v1/clinic")
 */
class ClinicController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * Needed for client-side navigation after initial page load.
     */
    public function clinics(Request $request)
    {
//        $clinics = $this->getDoctrine()->getRepository(Clinic::class)->findAll();
        $clinics = $this->getDoctrine()->getRepository(Clinic::class)->findAllClinic();

        return new JsonResponse($this->normalizeObjectsJson($clinics));
    }

    /**
     * @Route("/{id}")
     * @Method("GET")
     *
     * @param int $id
     * @param Request $request
     *
     * @return JsonResponse
     *
     * Needed for client-side navigation after initial page load
     */
    public function clinic($id, Request $request)
    {
        $clinics = $this->getDoctrine()->getRepository(Clinic::class)->find($id);

        return new JsonResponse($this->normalizeObjectsJson($clinics));
    }
}