<?php
/**
 * Base class for Api Controller.
 */

namespace AppBundle\Controller\Frontend\Api\v1;


use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class Controller extends BaseController
{
    /**
     * Json normalize objects.
     *
     * @param mixed $data
     * @return mixed
     */
    protected function normalizeObjectsJson($data)
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getName();
        });

        $serializer = new Serializer(array($normalizer), array($encoder));
        return $serializer->normalize($data);
    }
}