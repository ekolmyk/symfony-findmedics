<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Entity\Taxonomy;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Taxonomy controller.
 *
 * @Route("taxonomy")
 */
class TaxonomyController extends Controller
{
    /**
     * Lists all taxonomy entities.
     *
     * @Route("/", name="taxonomy_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $taxonomies = $em->getRepository('AppBundle:Taxonomy')->findAll();

        return $this->render('taxonomy/index.html.twig', array(
            'taxonomies' => $taxonomies,
        ));
    }

    /**
     * Creates a new taxonomy entity.
     *
     * @Route("/new", name="taxonomy_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $taxonomy = new Taxonomy();
        $form = $this->createForm('AppBundle\Form\TaxonomyType', $taxonomy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($taxonomy);
            $em->flush();

            return $this->redirectToRoute('taxonomy_show', array('id' => $taxonomy->getId()));
        }

        return $this->render('taxonomy/new.html.twig', array(
            'taxonomy' => $taxonomy,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a taxonomy entity.
     *
     * @Route("/{id}", name="taxonomy_show")
     * @Method("GET")
     */
    public function showAction(Taxonomy $taxonomy)
    {
        $deleteForm = $this->createDeleteForm($taxonomy);

        return $this->render('taxonomy/show.html.twig', array(
            'taxonomy' => $taxonomy,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing taxonomy entity.
     *
     * @Route("/{id}/edit", name="taxonomy_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Taxonomy $taxonomy)
    {
        $deleteForm = $this->createDeleteForm($taxonomy);
        $editForm = $this->createForm('AppBundle\Form\TaxonomyType', $taxonomy);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('taxonomy_edit', array('id' => $taxonomy->getId()));
        }

        return $this->render('taxonomy/edit.html.twig', array(
            'taxonomy' => $taxonomy,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a taxonomy entity.
     *
     * @Route("/{id}", name="taxonomy_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Taxonomy $taxonomy)
    {
        $form = $this->createDeleteForm($taxonomy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($taxonomy);
            $em->flush();
        }

        return $this->redirectToRoute('taxonomy_index');
    }

    /**
     * Creates a form to delete a taxonomy entity.
     *
     * @param Taxonomy $taxonomy The taxonomy entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Taxonomy $taxonomy)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('taxonomy_delete', array('id' => $taxonomy->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
