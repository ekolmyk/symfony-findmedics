<?php
namespace AppBundle\Controller\Backend;

use AppBundle\Entity\Clinic;
use AppBundle\Entity\Doctor;
use AppBundle\Form\ClinicMediaType;
use AppBundle\Form\ProfileType;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Twig\Profiler\Profile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Form;
use EasyCorp\Bundle\EasyAdminBundle\Form\Util\LegacyFormHelper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class ClinicController
 *
 * @Route("admin/clinic")
 * @package AppBundle\Controller
 */
class ClinicController extends BaseAdminController
{
    /**
     * @param \AppBundle\Entity\Clinic $entity
     * @param $view
     *
     * @return \Symfony\Component\Form\FormBuilder
     */
    public function createClinicEntityFormBuilder(Clinic $entity, $view)
    {
        $formBuilder = parent::createEntityFormBuilder($entity, $view);

        $formBuilder->add('profile', ProfileType::class, array('label' => false));
        $formBuilder->add('media', CollectionType::class, array(
            'entry_type' => ClinicMediaType::class,
            'entry_options' => ['label'=>false],
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            )
        );


        $easyadmin = $this->request->attributes->get('easyadmin');

        switch($easyadmin['view']) {
            case 'edit':
            case 'new':
                if ($formBuilder->has('doctors')) {
                    $formBuilder->remove('doctors');
                }
                break;
        }

        return $formBuilder;
    }

    /**
     * @param string $entityClass
     * @param int $page
     * @param int $maxPerPage
     * @param null $sortField
     * @param null $sortDirection
     * @param null $dqlFilter
     *
     * @return mixed
     */
    protected function findAll($entityClass, $page = 1, $maxPerPage = 15, $sortField = null, $sortDirection = null, $dqlFilter = null)
    {
        if (empty($sortDirection) || !in_array(strtoupper($sortDirection), array('ASC', 'DESC'))) {
            $sortDirection = 'DESC';
        }
        $queryBuilder = $this->executeDynamicMethod('create<EntityName>ListQueryBuilder', array($entityClass, $sortDirection, $sortField, $dqlFilter));

        $this->dispatch(EasyAdminEvents::POST_LIST_QUERY_BUILDER, array(
            'query_builder' => $queryBuilder,
            'sort_field' => $sortField,
            'sort_direction' => $sortDirection,
        ));

        return $this->get('easyadmin.paginator')->createOrmPaginator($queryBuilder, $page, $maxPerPage);
    }

    /**
     * @param $entity
     */
    function preUpdateClinicEntity($entity){
        foreach ($entity->getMedia() as $media){
            $media->setClinic($entity);
        }
    }

    /**
     * @param $entity
     */
    function prePersistClinicEntity($entity){
        foreach ($entity->getMedia() as $media){
            $media->setClinic($entity);
        }
    }
}