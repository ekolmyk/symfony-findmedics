<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Clinic
 *
 * @ORM\Table(name="clinic")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClinicRepository")
 */
class Clinic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255)
     */
    private $website;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Profile", cascade={"persist"})
     */
    protected $profile;

    /**
     * One Clinic has Many Doctors.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Doctor", mappedBy="clinic", cascade={"persist"})
     */
    private $doctors;

    /**
     * Many Clinics has Many Features.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Feature")
     */
    protected $features;

    /**
     * Many Clinics has Many Illnes.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Illnes")
     */
    protected $illnes;

    /**
     * Many Clinics has Many Treatments.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Treatments")
     */
    protected $treatments;

    /**
     * Many Clinics has Many Treatments.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tests")
     */
    protected $tests;


    /**
     * One Clinic has Many Doctors.
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ClinicMedia", mappedBy="clinic", cascade={"persist"})
     */
    public $media;

    /**
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

    /**
     * Clinic constructor.
     */
    public function __construct() {
        $this->doctors = new ArrayCollection();
        $this->features = new ArrayCollection();
        $this->illnes = new ArrayCollection();
        $this->treatments = new ArrayCollection();
        $this->tests = new ArrayCollection();
        $this->media = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Clinic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Clinic
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Get profile
     *
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set profile
     *
     * @param \AppBundle\Entity\Profile|null $profile
     */
    public function setProfile(Profile $profile = null)
    {
        $this->profile = $profile;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Clinic
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Clinic
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get image
     *
     * @return mixed
     */
    public function getImage()
    {
        return $this->getProfile()->getImage();
    }

    /**
     * Add doctor
     *
     * @param \AppBundle\Entity\Doctor $doctor
     *
     * @return Clinic
     */
    public function addDoctor(\AppBundle\Entity\Doctor $doctor)
    {
        $this->doctors[] = $doctor;
        $doctor->setClinic($doctor);

        return $this;
    }

    /**
     * Remove doctor
     *
     * @param \AppBundle\Entity\Doctor $doctor
     */
    public function removeDoctor(\AppBundle\Entity\Doctor $doctor)
    {
        $this->doctors->removeElement($doctor);
    }

    /**
     * Get doctors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoctors()
    {
        return $this->doctors;
    }

    /**
     * Add feature
     *
     * @param \AppBundle\Entity\Feature $feature
     *
     * @return Clinic
     */
    public function addFeature(\AppBundle\Entity\Feature $feature)
    {
        $this->features[] = $feature;

        return $this;
    }

    /**
     * Remove feature
     *
     * @param \AppBundle\Entity\Feature $feature
     */
    public function removeFeature(\AppBundle\Entity\Feature $feature)
    {
        $this->features->removeElement($feature);
    }

    /**
     * Get features
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * Add illne
     *
     * @param \AppBundle\Entity\Illnes $illne
     *
     * @return Clinic
     */
    public function addIllne(\AppBundle\Entity\Illnes $illne)
    {
        $this->illnes[] = $illne;

        return $this;
    }

    /**
     * Remove illne
     *
     * @param \AppBundle\Entity\Illnes $illne
     */
    public function removeIllne(\AppBundle\Entity\Illnes $illne)
    {
        $this->illnes->removeElement($illne);
    }

    /**
     * Get illnes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIllnes()
    {
        return $this->illnes;
    }

    /**
     * Add treatment
     *
     * @param \AppBundle\Entity\Treatments $treatment
     *
     * @return Clinic
     */
    public function addTreatment(\AppBundle\Entity\Treatments $treatment)
    {
        $this->treatments[] = $treatment;

        return $this;
    }

    /**
     * Remove treatment
     *
     * @param \AppBundle\Entity\Treatments $treatment
     */
    public function removeTreatment(\AppBundle\Entity\Treatments $treatment)
    {
        $this->treatments->removeElement($treatment);
    }

    /**
     * Get treatments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTreatments()
    {
        return $this->treatments;
    }

    /**
     * Add test
     *
     * @param \AppBundle\Entity\Treatments $test
     *
     * @return Clinic
     */
    public function addTest(\AppBundle\Entity\Treatments $test)
    {
        $this->tests[] = $test;

        return $this;
    }

    /**
     * Remove test
     *
     * @param \AppBundle\Entity\Treatments $test
     */
    public function removeTest(\AppBundle\Entity\Treatments $test)
    {
        $this->tests->removeElement($test);
    }

    /**
     * Get tests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTests()
    {
        return $this->tests;
    }

    /**
     * Add medium
     *
     * @param \AppBundle\Entity\ClinicMedia $media
     *
     * @return Clinic
     */
    public function addMedia(\AppBundle\Entity\ClinicMedia $media)
    {
        $this->media[] = $media;
        $media->setClinic($media);

        return $this;
    }

    /**
     * Remove medium
     *
     * @param \AppBundle\Entity\ClinicMedia $media
     */
    public function removeMedia(\AppBundle\Entity\ClinicMedia $media)
    {
        $this->media->removeElement($media);
    }

    /**
     * Get media
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedia()
    {
        return $this->media;
    }
}
