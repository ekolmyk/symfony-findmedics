<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;


/**
 * Profile
 *
 * @ORM\Table(name="profile")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProfileRepository")
 */
class Profile extends Entity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Assert\NotBlank()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=80)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="overview", type="text", nullable=true)
     */
    private $overview = null;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", nullable=true)
     */
    private $postcode = null;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100, nullable=true)
     */
    private $password = null;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="boolean", length=10)
     */
    private $status;

    /**
     * Many Profiles have Many Specialties.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Specialty")
     */
    private $specialties;

    /**
     * @var bool
     *
     * @ORM\Column(name="available_24_hours", type="boolean")
     */
    private $available24Hours;

    /**
     * @var bool
     *
     * @ORM\Column(name="online_consultation", type="boolean")
     */
    private $onlineConsultation;

    /**
     * @var bool
     *
     * @ORM\Column(name="does_not_require_gp_referral", type="boolean")
     */
    private $doesNotRequireGPreferral;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * Profile constructor.
     */
    public function __construct() {
        $this->taxonomyTerm = new ArrayCollection();
        $this->specialties = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->login;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Profile
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Profile
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Profile
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Profile
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set overview
     *
     * @param string $overview
     *
     * @return Profile
     */
    public function setOverview($overview)
    {
        $this->overview = $overview;

        return $this;
    }

    /**
     * Get overview
     *
     * @return string
     */
    public function getOverview()
    {
        return $this->overview;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Profile
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Add specialty
     *
     * @param \AppBundle\Entity\Specialty $specialty
     *
     * @return Profile
     */
    public function addSpecialty(\AppBundle\Entity\Specialty $specialty)
    {
        $this->specialties[] = $specialty;

        return $this;
    }

    /**
     * Remove specialty
     *
     * @param \AppBundle\Entity\Specialty $specialty
     */
    public function removeSpecialty(\AppBundle\Entity\Specialty $specialty)
    {
        $this->specialties->removeElement($specialty);
    }

    /**
     * Get specialties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialties()
    {
        return $this->specialties;
    }

    /**
     * Set available24Hours
     *
     * @param boolean $available24Hours
     *
     * @return Profile
     */
    public function setAvailable24Hours($available24Hours)
    {
        $this->available24Hours = $available24Hours;

        return $this;
    }

    /**
     * @return bool
     */
    public function getOnlineConsultation()
    {
        return $this->onlineConsultation;
    }

    /**
     * @param bool $onlineConsultation
     */
    public function setOnlineConsultation($onlineConsultation)
    {
        $this->onlineConsultation = $onlineConsultation;
    }

    /**
     * @return bool
     */
    public function getDoesNotRequireGPreferral()
    {
        return $this->doesNotRequireGPreferral;
    }

    /**
     * @param bool $doesNotRequireGPreferral
     */
    public function setDoesNotRequireGPreferral($doesNotRequireGPreferral)
    {
        $this->doesNotRequireGPreferral = $doesNotRequireGPreferral;
    }

    /**
     * Get available24Hours
     *
     * @return bool
     */
    public function getAvailable24Hours()
    {
        return $this->available24Hours;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Profile
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Profile
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param string $created
     *
     * @return Profile
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Profile
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Profile
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Profile
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
    }

    /**
     * @ORM\PreUpdate
     */
    public function onUpdate()
    {
        $this->updated = new \DateTime("now");
    }
}
