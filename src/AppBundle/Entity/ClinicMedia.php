<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClinicMedia
 *
 * @ORM\Table(name="clinic_media")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClinicMediaRepository")
 */
class ClinicMedia extends Entity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Clinic", inversedBy="media")
     * @ORM\JoinColumn(name="clinic_id", referencedColumnName="id")
     */
    private $clinic;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ClinicMedia
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ClinicMedia
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Set clinic
     *
     * @param \AppBundle\Entity\Clinic $clinic
     *
     * @return ClinicMedia
     */
    public function setClinic(\AppBundle\Entity\Clinic $clinic = null)
    {
        $this->clinic = $clinic;

        return $this;
    }

    /**
     * Get clinic
     *
     * @return \AppBundle\Entity\Clinic
     */
    public function getClinic()
    {
        return $this->clinic;
    }
}
