<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\Request;

/**
 * Doctor
 *
 * @ORM\Table(name="doctor")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DoctorRepository")
 */
class Doctor extends Entity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_number", type="string", length=45, unique=true)
     */
    private $registrationNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="education", type="text")
     */
    private $education;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="career_start", type="date")
     */
    private $careerStart;

    /**
     * @var bool
     *
     * @ORM\Column(name="featured", type="boolean")
     */
    private $featured;

    /**
     * @var string
     *
     * @ORM\Column(name="appointment_duration", type="string", length=45)
     */
    private $appointmentDuration;

    /**
     * @var string
     *
     * @ORM\Column(name="speciality", type="string", length=255)
     */
    private $speciality;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Clinic", inversedBy="doctors")
     * @ORM\JoinColumn(name="clinic_id", referencedColumnName="id")
     */
    private $clinic;

    /**
     * @return string
     */
    public function __toString() {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Doctor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set registrationNumber
     *
     * @param string $registrationNumber
     *
     * @return Doctor
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * Get registrationNumber
     *
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * Set education
     *
     * @param string $education
     *
     * @return Doctor
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get education
     *
     * @return string
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * Set careerStart
     *
     * @param \DateTime $careerStart
     *
     * @return Doctor
     */
    public function setCareerStart($careerStart)
    {
        $this->careerStart = $careerStart;

        return $this;
    }

    /**
     * Get careerStart
     *
     * @return \DateTime
     */
    public function getCareerStart()
    {
        return $this->careerStart;
    }

    /**
     * Set featured
     *
     * @param boolean $featured
     *
     * @return Doctor
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Get featured
     *
     * @return bool
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Set appointmentDuration
     *
     * @param string $appointmentDuration
     *
     * @return Doctor
     */
    public function setAppointmentDuration($appointmentDuration)
    {
        $this->appointmentDuration = $appointmentDuration;

        return $this;
    }

    /**
     * Get appointmentDuration
     *
     * @return string
     */
    public function getAppointmentDuration()
    {
        return $this->appointmentDuration;
    }

    /**
     * Set speciality
     *
     * @param string $speciality
     *
     * @return Doctor
     */
    public function setSpeciality($speciality)
    {
        $this->speciality = $speciality;

        return $this;
    }

    /**
     * Get speciality
     *
     * @return string
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Doctor
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Doctor
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Set clinic
     *
     * @param \AppBundle\Entity\Clinic $clinic
     *
     * @return Doctor
     */
    public function setClinic(\AppBundle\Entity\Clinic $clinic = null)
    {
        $this->clinic = $clinic;

        return $this;
    }

    /**
     * Get clinic
     *
     * @return \AppBundle\Entity\Clinic
     */
    public function getClinic()
    {
        return $this->clinic;
    }
}
