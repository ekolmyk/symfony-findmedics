import React, { Component } from 'react';
// import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Button, Icon } from 'react-materialize';
// import { selectForm } from "../../actions/actionContactForm"


class Details extends Component {
    render() {
        if (!this.props.testLinkDetails) {
            return (<Button waves='light'>Fill the form<Icon right>vertical_align_top</Icon></Button>)
        }
        return(
            <Modal
                header='Modal Header'
                bottomSheet
                trigger={<Button>MODAL BUTTOM SHEET STYLE<Icon right>send</Icon></Button>}>
                <p>{this.props.testLinkDetails.desc}</p>
            </Modal>
        )
    }
}



function mapStateToProps (state) {
    return {
        testLinkDetails: state.dataForm_active
    };
}


export default connect(mapStateToProps)(Details);
