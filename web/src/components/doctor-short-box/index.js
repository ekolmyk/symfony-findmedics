import React from 'react';

const DoctorShortBox = (props) => {
    let consultation = '';
    if (props.onlineConsultation == true){
        consultation = "yes"
    } else {
        consultation = 'no'
    }

    let preferral = '';
    if (props.doesNotRequireGPreferral == true){
        preferral = "yes"
    } else {
        preferral = 'no'
    }

    let available = '';
    if (props.available24Hours == true){
        available = "yes"
    } else {
        available = 'no'
    }

    return(
        <div className="collection">
            <a href="#!" className="collection-item">
                <span className="badge">{props.exp}</span>Experience
            </a>
            <a href="#!" className="collection-item">
                <span className="badge">{preferral}</span>Not require regular referal
            </a>
            <a href="#!" className="collection-item">
                <span className="badge">{consultation}</span>Online Consultation
            </a>
            <a href="#!" className="collection-item">
                <span className="badge">{available}</span>Available 24h
            </a>
        </div>
    )
}

export default DoctorShortBox;
