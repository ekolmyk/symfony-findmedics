import React, { Component } from 'react';
import { Row, Col } from 'react-materialize';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ButtonCustom from '../custom-elements/ButtonCustom';
import SelectCustom from '../custom-elements/SelectCustom';

import { selectTimeData } from '../../actions/BookAppointment_action';
import BookAppointment_TimeButton from './BookAppointment_TimeButton';


class BookAppointment extends Component {
    constructor(props){
        super(props)
        this.state = {
            practice : '',
            location : '',
            time: '',
            modalOpened: false
        }
        this.handleSelectChange = this.handleSelectChange.bind(this)
        this.modalToggle = this.modalToggle.bind(this)
    }
    modalToggle (event) { //SubmitForm
        event.preventDefault();
        if (!this.props.BookAppointmentTimeAlone) {
            this.setState({
                modalOpened: !this.state.modalOpened
            });
            return false;
        } else {
            this.setState({
                modalOpened: !this.state.modalOpened,
                time: (this.props.BookAppointmentTimeAlone.dateIt + ' ' + this.props.BookAppointmentTimeAlone.timeIt)
            });
            console.log("Practice: ", this.state.practice);
            console.log("Time: ", this.state.time);
        }
    }
    handleSelectChange(event){
        this.setState({practice: event.target.value});
        console.log('PracticeSelected:', this.state.practice);
    }

    render() {
        const modalContainerClass = this.state.modalOpened ? 'modalContainer is-open' : 'modalContainer'
        return(
            <div className="card bookAppointment z-depth-1">
                <h4 className="center red-text">Book appointment</h4>
                <form className="section" onSubmit={this.modalToggle}>
                    <SelectCustom
                        onChange={this.handleSelectChange}
                        value={this.props.practice}
                        defaultValue="Select practice"
                        options='Option 1, Option 2, Option 3'
                    />

                    <p className="valign-wrapper">
                        <i className="material-icons">location_on</i>
                        {/*<span> Location address: {this.props.location.city}</span>*/}
                        <span> Location address: test-text</span>
                    </p>

                    <BookAppointment_TimeButton />

                    <Row>
                        <Col s={12} className='center input-field'>
                            <ButtonCustom
                            type="submit"
                            text="See all appointment"
                            name="action"
                            className="waves-effect waves-light blue lighten-1"
                            />
                            <div className={modalContainerClass}
                                 onClick={this.modalToggle}>
                                <div className='modal modalbBox is-active'>
                                    <div className="modal-content">
                                        <div className='center'>Select practice: {this.state.practice || 'nothing selected'}</div>
                                        {/*<div className='center'>Location address: {this.props.location.city}</div>*/}
                                        <div className='center'>Location address: test-text</div>
                                        <div className='center'>Time of visit: {this.state.time || 'nothing selected'}</div>
                                    </div>
                                    <div className="modal-footer">
                                        <ButtonCustom
                                            onClick={this.modalToggle}
                                            text="Close"
                                            name="action"
                                            className="waves-effect waves-light"
                                        />
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </form>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        BookAppointmentTime: state.selectTimeData,
        BookAppointmentTimeAlone: state.dataBookAppointment_active
    };
}

function matchDispatchToProps (dispatch) {
    return bindActionCreators({selectTimeData: selectTimeData}, dispatch)
}


export default connect(mapStateToProps, matchDispatchToProps)(BookAppointment);
