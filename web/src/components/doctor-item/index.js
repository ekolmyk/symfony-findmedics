import React, { Component } from 'react';
import RateComponent from '../../components/rateing';
import {
    Link
} from 'react-router-dom';

import { connect } from 'react-redux';
import { itemsFetchData } from '../../actions/doctor-items';


class DoctorItem extends Component {
    componentDidMount() {
        this.props.fetchData('http://symfony-findmedics.loc/api/v1/clinic/');
    }

    showListClinic() {
        return this.props.items.map((item) => {
            let clinic_rate = item.id;
            //let doctor_item_type = '';
            //if (this.props.type === 'doctor') {
            //    doctor_item_type = (<Link to={'/doctor/' + this.props.id}>Read More</Link>);
            //} else if (this.props.type === 'clinic') {
            let doctor_item_type = (<Link to={'/clinic/' + item.id}>Read More</Link>);
            //}
            let clinic_illnes = item.illnes.map((illness) => {
                return(
                    <li key={illness.id}><small>{illness.name}</small></li>
                );
            });
            // let clinicIllnes2 = item.illnes;
            // for (let i = 0; i < clinicIllnes2.length; i++) {
            //     console.log( clinicIllnes2[i].name );
            // }
            return(
                <div key={item.id} id={`clinicItem_${item.id}`} className="card doctorList-item">
                    <div className="card-content">
                        <div className="row">
                            <div className="col s12 m4">
                                <div>
                                    <img src={`http://symfony-findmedics.loc/uploads/images/entity/${item.image}`} alt=""/>
                                </div>
                            </div>
                            <div className="col s12 m8">
                                <p className="card-title grey-text text-darken-4">{item.name}</p>

                                {/*<p><small>Location: {this.props.location.name}</small></p>*/}
                                <p><small>Illness: </small></p>
                                <ol>{ clinic_illnes }</ol>
                                <p><small>Treatments: {item.overview}</small></p>
                            </div>
                        </div>
                    </div>
                    <div className="card-action">
                        {doctor_item_type}
                    </div>
                    <RateComponent rate={ clinic_rate } />
                </div>
            )
        });
    }

    render() {
        console.log('list:', this.props.items);

        if (this.props.hasErrored) {
            return <p>Sorry! There was an error loading the items</p>;
        }

        if (this.props.isLoading) {
            return <p>Loading…</p>;
        }

        return(
            <div className="card doctorList-box">
                {this.showListClinic()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items,
        hasErrored: state.itemsHasErrored,
        isLoading: state.itemsIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(itemsFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DoctorItem);
