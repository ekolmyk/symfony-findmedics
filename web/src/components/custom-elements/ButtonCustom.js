import React, { Component } from 'react';

class ButtonCustom extends Component {
    render() {
        return (
            <button
                onClick={this.props.onClick || null}
                className={
                    this.props.className
                        ? `btn ${this.props.className}`
                        : "btn"
                }
                type={this.props.type || "button"}
                value={this.props.value || null}
                name={this.props.name || null}
            >
                {this.props.text}
            </button>
        );
    }
}

export default ButtonCustom;
