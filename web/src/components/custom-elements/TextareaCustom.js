import React, { Component } from 'react';
import { Row } from 'react-materialize';


class LabelCustom extends Component {
    render() {
        if (this.props.hasLabel === "true") {
            return (
                <label htmlFor={this.props.htmlFor}>{this.props.label}</label>
            );
        } else if (this.props.activeLabel === "active") {
            return (
                <label className="active" htmlFor={this.props.htmlFor}>{this.props.label}</label>
            );
        } else {
            return (
                <label></label>
            );
        }
    }
}

class TextareaCustom extends Component {
    render() {
        return (
            <Row>
                <div className="input-field col s12">
                    <textarea
                        className={this.props.class || "materialize-textarea"}
                        cols={this.props.cols || null}
                        id={this.props.htmlFor}
                        name={this.props.name || null}
                        required={this.props.required || null}
                        rows={this.props.rows || null}
                    />
                    <LabelCustom
                        hasLabel={this.props.hasLabel}
                        htmlFor={this.props.htmlFor}
                        label={this.props.label}
                    />
                </div>
            </Row>
        );
    }
}

export default TextareaCustom;
