import React, { Component } from 'react';
import { Row } from 'react-materialize';
import { Field } from 'redux-form';


class LabelCustom extends Component {
    render() {
        if (this.props.hasLabel === "true") {
            return (
                <label htmlFor={this.props.htmlFor}>{this.props.label}</label>
            );
        } else if (this.props.activeLabel === "active") {
            return (
                <label className="active" htmlFor={this.props.htmlFor}>{this.props.label}</label>
            );
        } else {
            return (
                <label></label>
            );
        }
    }
}

class FieldCustom extends Component {
    render() {
        return (
            <Row>
                <div className="input-field col s12">
                    <Field
                        onChange={this.props.onChange || null}
                        name={this.props.name}
                        component={this.props.component || "input"}
                        type={this.props.type || "text"}
                        value={this.props.value || ""}
                        className={this.props.class || "validate"}
                        id={this.props.htmlFor}
                        max={this.props.max || null}
                        min={this.props.min || null}
                        placeholder={this.props.placeholder || null}
                        required={this.props.required || null}
                        step={this.props.step || null}
                    />
                    <LabelCustom
                        activeLabel={this.props.activeLabel}
                        hasLabel={this.props.hasLabel}
                        htmlFor={this.props.htmlFor}
                        label={this.props.label}
                    />
                </div>
            </Row>
        );
    }
}

export default FieldCustom;
