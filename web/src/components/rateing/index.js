import React, { Component } from 'react';

class RateComponent extends Component {
    render() {
        let valueRate = this.props.rate;
        let stars_val = '';
        if (valueRate === 0) {
            stars_val = 'star_border star_border star_border star_border star_border'
        } else if (valueRate >= 0.5 && valueRate < 1) {
            stars_val = 'star_half star_border star_border star_border star_border'
        } else if (valueRate >= 1 && valueRate < 1.5) {
            stars_val = 'star star_border star_border star_border star_border'
        } else if (valueRate >= 1.5 && valueRate < 2) {
            stars_val = 'star star_half star_border star_border star_border'
        } else if (valueRate >= 2 && valueRate < 2.5) {
            stars_val = 'star star star_border star_border star_border'
        } else if (valueRate >= 2.5 && valueRate < 3) {
            stars_val = 'star star star_half star_border star_border'
        } else if (valueRate >= 3 && valueRate < 3.5) {
            stars_val = 'star star star star_border star_border'
        } else if (valueRate >= 3.5 && valueRate < 4) {
            stars_val = 'star star star star_half star_border'
        } else if (valueRate >= 4 && valueRate < 4.5) {
            stars_val = 'star star star star star_border'
        } else if (valueRate >= 4.5 && valueRate < 5) {
            stars_val = 'star star star star star_half'
        } else if (valueRate === 5) {
            stars_val = 'star star star star star'
        }

        return(
            <div className="doctorList-item-rate">
                <span>{valueRate}</span>
                <i className="material-icons">{stars_val}</i>
            </div>
        )
    }
}

export default RateComponent;
