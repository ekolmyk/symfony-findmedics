import React, { Component } from 'react';
import DoctorItem from '../../components/doctor-item';


class DoctorList extends Component {
    render() {
        return(
            <section className="doctorList">
                <DoctorItem />
            </section>
        )
    }
}

export default DoctorList;