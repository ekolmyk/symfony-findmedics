import React, { Component } from 'react';
import RateComponent from './components/rateing';

class DoctorIntro extends Component {
    constructor(props) {
        super(props);
        this.makeAppointment = this.makeAppointment.bind(this);
    }

    makeAppointment() {
        alert('good job');
    }

    render() {
        let clinicRate = 4;
        return(
            <div className="card pageInner">
                <div className="card-content">
                    <div className="row">
                        <div className="col s12 m2">
                            <div>
                                <img src={`/uploads/images/entity/${this.props.item.image}`} alt=""/>
                            </div>
                        </div>
                        <div className="col s12 m8">
                            <p className="card-title grey-text text-darken-4">{this.props.item.name}</p>
                            <p><small>Website: {this.props.item.website}</small></p>
                             <RateComponent rate={ clinicRate } />
                            <p><strong>Next available appointment</strong></p>
                            <div className="appointmentBlock">
                                <div className="appointmentBlock-item">
                                    <div className="chip appointmentBlock-item-time">
                                        <img src="http://placehold.it/20x20" alt="Contact Person" />
                                        Today 14:30
                                    </div>
                                    <div className="chip appointmentBlock-item-book" onClick={this.makeAppointment}>
                                        Book Now
                                        <i className="material-icons">event_available</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DoctorIntro;
