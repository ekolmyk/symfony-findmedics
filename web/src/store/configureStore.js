// import { createStore, applyMiddleware, compose } from 'redux';
//
// const middlewares = [
//     thunk
// ];
//
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const myStore = createStore(reducer, composeEnhancers(
//     applyMiddleware(...middlewares)
// ));

import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import allRedusers from '../reducers';

export default function configureStore(initialState) {
    return createStore(
        allRedusers,
        initialState,
        applyMiddleware(thunk)
    );
}