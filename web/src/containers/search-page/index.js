import React, { Component } from 'react';
import firebase from 'firebase';
import DoctorList from '../../components/doctor-listing';
import MapComponent from '../../MapComponent';
import SearchRefine from '../../components/search-refine';
import AdComponent from '../../components/advertisement';
import $ from 'jquery';


class SearchPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            banner: ""
        };
    }

    loadAd() {
        const ref = new firebase.database().ref('main_ad/');
        ref.on('value', function(snapshot) {
            const banner = snapshot.val();

            this.setState({
                banner: banner
            });
        }.bind(this));
    }

    componentWillMount() {
        //console.log('componentWillMount')
    }

    componentDidMount() {
        //this.loadData(1);
        this.loadData();
        this.loadAd();
        // fetch('http://symfony-findmedics.loc/api/clinics').then(results => { return results.json(); }).then(data => {
        //     debugger;
        //     this.setState({ items: data })
        //     console.log('state', this.state.items)
        // });
        //console.log('componentDidMount')
    }

    loadData() {
        $.getJSON('http://symfony-findmedics.loc/api/v1/clinic')
            .then( results  => {
                this.setState({items: results})
            });
    }

    // getInitialState() {
    //     return {
    //         items: []
    //         //formDisplayed: false,
    //     }
    // }

    render() {
        return(
            <div className="container">
                <div className="row">
                    <div className="col s12 m4">
                        <div className="card">
                            <div className="card-content">
                                <div id="map">
                                    <MapComponent items={this.state.items} />
                                </div>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-content">
                                <SearchRefine />
                            </div>
                        </div>
                        <div className="card">
                            <AdComponent banner={this.state.banner} />
                        </div>
                    </div>
                    <div className="col s12 m8">
                        <DoctorList />
                    </div>
                </div>
            </div>
        )
    }
}

export default SearchPage;
