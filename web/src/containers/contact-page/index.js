import React, { Component } from 'react';
import { connect } from 'react-redux';


import ContactForm from '../../components/contact-form/index';

class ContactPage extends Component{
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col s12">
                        <h2 className="center">
                            Contact us
                        </h2>
                    </div>
                    <div className="col s12 m7 l5">
                        <div className="row">
                            <div className="col s12">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin a congue nunc. Curabitur consectetur tortor est, ut dapibus lectus fermentum sed.
                                    Aliquam maximus, libero quis fermentum vehicula, massa felis facilisis nisi, nec dignissim elit lorem non justo. In vel ultrices metus, at scelerisque massa.
                                    Nulla finibus est in ante rutrum faucibus. Phasellus at consectetur massa. Suspendisse eget elementum purus, sit amet imperdiet velit.
                                    Sed blandit iaculis dui, a tristique libero tincidunt vel. Sed gravida velit mauris, ac feugiat dui maximus eget. In in massa sapien.
                                </p>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>General enquiries</td>
                                        <td>
                                            <a href="mailto:support@findmedics.co.uk">support@findmedics.co.uk</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Partnership with clinics</td>
                                        <td>
                                            <a href="mailto:sales@findmedics.co.uk">sales@findmedics.co.uk</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <p>
                                    Proin at imperdiet enim. Nulla facilisi. Suspendisse cursus urna et nisl fringilla, vitae vehicula libero rutrum.
                                </p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col s12">
                                {JSON.stringify(this.props.testValueForm) ? <pre>{JSON.stringify(this.props.testValueForm.values)}</pre> : 'Please fill in the form!'}
                            </div>
                        </div>
                    </div>
                    <ContactForm />
                </div>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        testValueForm: state.form.contactForm
    };
}

export default connect(mapStateToProps)(ContactPage);

// export default ContactPage;
