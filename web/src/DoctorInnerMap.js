import React, { Component } from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

export class DoctorInnerMap extends Component {
    onMarkerClick(props, marker, e) {
        console.log(marker);
        console.log(props);
    }

    render() {
        // const markerItem = this.props.item.map(function (item) {
        //     return <Marker
        //         //key={item.id}
        //         key={1}
        //         //id={item.id}
        //         id={1}
        //         //name={item.name}
        //         name={ name }
        //         title={ name }
        //         //title={item.name}
        //         //position={{lat: item.profile.created.timezone.location.latitude, lng: item.profile.created.timezone.location.longitude}}
        //         position={{lat: 55.75583, lng: 37.61777}}
        //         //onClick={this.onMarkerClick}
        //     />
        // }.bind(this));
        return (
            <Map
                google={this.props.google}
                zoom={14}
                clickableIcons={false}
                initialCenter={{
                    lat: 55.75583,
                    lng: 37.61777
                }}
            >

                {/*{markerItem}*/}

                <Marker
                    //key={item.id}
                    key={1}
                    //id={item.id}
                    id={1}
                    //name={item.name}
                    // name={ name }
                    // title={ name }
                    //title={item.name}
                    //position={{lat: item.profile.created.timezone.location.latitude, lng: item.profile.created.timezone.location.longitude}}
                    position={{lat: 55.75583, lng: 37.61777}}
                    //onClick={this.onMarkerClick}
                />

                <InfoWindow onClose={this.onInfoWindowClose}>
                    <div>
                        <h1>1</h1>
                    </div>
                </InfoWindow>
            </Map>
        );
    }
}
//export class MapContainer extends Component {}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyB8YZdLwCpE2aS6ks55Opn9oeTz_UNmFWI',
    version: '3.28'
})(DoctorInnerMap)

// const WrappedContainer = GoogleApiWrapper({
//     apiKey: 'AIzaSyB8YZdLwCpE2aS6ks55Opn9oeTz_UNmFWI',
// })(MapContainer);
// export default WrappedContainer
