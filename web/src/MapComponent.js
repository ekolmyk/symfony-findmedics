import React, { Component } from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

export class MapContainer extends Component {
    onMarkerClick(props, marker, e) {
        console.log(marker);
        console.log(props);
    }

    render() {
        const markerItem = this.props.items.map(function (item) {
            return <Marker
                key={item.id}
                id={item.id}
                name={item.name}
                title={item.name}
                // position={{lat: item.profile.created.timezone.location.latitude, lng: item.profile.created.timezone.location.longitude}}
                onClick={this.onMarkerClick}
            />
        }.bind(this));
        return (
            <Map
                google={this.props.google}
                zoom={14}
                clickableIcons={false}
                initialCenter={{
                    lat: 55.75583,
                    lng: 37.61777
                }}
            >

                {markerItem}

                <InfoWindow onClose={this.onInfoWindowClose}>
                    <div>
                        <h1>1</h1>
                    </div>
                </InfoWindow>
            </Map>
        );
    }
}
//export class MapContainer extends Component {}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyB8YZdLwCpE2aS6ks55Opn9oeTz_UNmFWI',
    version: '3.28'
})(MapContainer)

// const WrappedContainer = GoogleApiWrapper({
//     apiKey: 'AIzaSyB8YZdLwCpE2aS6ks55Opn9oeTz_UNmFWI',
// })(MapContainer);
// export default WrappedContainer
