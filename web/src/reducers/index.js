import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import dataBookAppointmentToday from './BookAppointment_dataToday';
import dataBookAppointmentTomorrow from './BookAppointment_dataTomorrow';
import dataBookAppointmentActive  from './BookAppointment_dataActive';
import testStoreTest from './TestStore';
// import { items, itemsHasErrored, itemsIsLoading } from './items';
import { items, itemsHasErrored, itemsIsLoading } from './doctor-items';

// import contactFormReducer from './ContactForm_reduser';
// import { listing } from './listing';
//
// export const rootReducer = combineReducers({
//     listing
// });


const allRedusers = combineReducers ({
    routing: routerReducer,

    items,
    itemsHasErrored,
    itemsIsLoading,

    form: formReducer,
    // contactFormReducer: contactFormReducer,
    dataBookAppointmentToday: dataBookAppointmentToday,
    dataBookAppointmentTomorrow: dataBookAppointmentTomorrow,
    dataBookAppointment_active: dataBookAppointmentActive,
    testStoreLive: testStoreTest
});

export default allRedusers;