export default function (state=null, action) {
    switch (action.type) {
        case "TIME_DATA":
            return action.payload;
        default:
            return state;
    }
}
