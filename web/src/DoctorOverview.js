import React, { Component } from 'react';
import DoctorShortBox from './components/doctor-short-box';

class DoctorOverview extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let currItem = this.props.item;
        let profile = this.props.profile;
        let overviewContent = '';
        let emailContent = '';
        let phoneContent = '';
        let overviewShortBox = '';
        let illnesList = '';
        let treatmentsList = '';
        console.log(profile);

        if (typeof profile !== 'undefined') {
            overviewContent = <p>{profile.overview}</p>;
            emailContent = <p>{profile.email}</p>;
            phoneContent = <p>{profile.phone}</p>;
            overviewShortBox = (
                <DoctorShortBox
                    key={profile.id}
                    exp="5"
                    onlineConsultation={profile.onlineConsultation}
                    doesNotRequireGPreferral = {profile.doesNotRequireGPreferral}
                    available24Hours = {profile.available24Hours}
                />
            )
            illnesList = this.props.item.illnes.map(function (val) {
                return <li key={val.id}>{val.name}</li>
            });
            treatmentsList = this.props.item.treatments.map(function (val) {
                return <li key={val.id}>{val.name}</li>
            });
        }

        return(
            <div className="overviewBlock" id="overview-block">
                <div className="row">
                    <div className="col s12 m8">
                        <h5>Overview:</h5>
                        {overviewContent}
                        <h5>Email:</h5>
                        {emailContent}
                        <h5>Phone:</h5>
                        {phoneContent}
                        <h5>Illnesses</h5>
                        <ul className="interestList">
                            {illnesList}
                        </ul>
                        <h5>Treatments</h5>
                        <ul className="interestList">
                            {treatmentsList}
                        </ul>
                    </div>
                    <div className="col s12 m4">
                        {overviewShortBox}
                    </div>
                </div>
            </div>
        )
    }
}

export default DoctorOverview;
