import React from 'react';
import ReactDOM from 'react-dom';
import { Provider }  from 'react-redux';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import configureStore from './store/configureStore';
import allRedusers from './reducers';
import App from './components/app/index';



const history = createHistory();// Create your chosen browser history

const middleware = routerMiddleware(history);// Create middleware to intercept and send navigation actions

const store = configureStore(); // You can also pass in an initialState here
console.log(store.getState(allRedusers));           // Condition of our "store"
store.subscribe(() => {                             // Subscription to changes in our "store", using the method "subscribe"
    console.log('subscribe', store.getState());     // Type of modified to our "store"
})



ReactDOM.render(
    <Provider store={ store }>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);